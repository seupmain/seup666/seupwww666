### Opis funkcji komponentu RPT
Komponent zawiera w sobie dedykowaną aplikację JasperReport Server.

###Technologie
- JasperReports Server 7.5.0
- Apache Tomcat 9

### Diagram komponentu 
```plantuml
@startuml c4
!includeurl https://raw.githubusercontent.com/RicardoNiepel/C4-PlantUML/master/C4_Component.puml

ContainerDb(db, "Postgres", "Baza danych")
Container(seupRpt, "seup-rpt", "JASPER")
Container(seupCma, "seup-cma", "CMA")

Rel(seupRpt, db, "Zapis i odczyt", "JDBC")
Rel(seupCma, seupRpt, "Używa REST API", "JSON/HTTP")
@enduml
```

### Uruchomienie aplikacji
#### Przed pierwszym uruchomieniem należy:
1. Utworzyć dedykowanego użytkownika bazy danych oraz samą baze danych
2. Ustawić parametry połaczenia do utwożonej bazy danych w Dockerfile
domyślna konfiguracja:
SEUP_JASPER_DB_HOST=postgres  
SEUP_JASPER_DB_PORT=5432  
SEUP_JASPER_DB_NAME=jasperserver    
SEUP_JASPER_DB_USER=jasper  
SEUP_JASPER_DB_PASSWORD=jasper
#### Uruchomienie kontenera
1. Zbudować obraz docker:
`docker build -t seup_rpt .`
2. Uruchomić obraz poleceniem:
`docker run seup_rpt`

Logi z instalacji JasperReports Server w kontenerze znajdują sie w `/usr/src/log/build.log`

####Konfiguracja instalacji
Reszta konfiguracji serwera JasperReports znajduje się w katalogu `docker`.

### Panel administratora
Panel administratora na środowiskach testowych znajduje się pod adresem https://adres_srodowiska/jasper (jasperadmin/jasperadmin)
Po pierwszym uruchomienu należy zmienić hasło dla użytkownika jasperadmin

### Aktualizacja raportów
1. Uruchamiamy lokalnie kontener z JasperServer
2. Modyfikujemy raporty przy użyciu np. JasperReports Studio
3. Zapisujemy zmiany
4. W panelu administratora JasperServer odnajdujemy opcję exportu:
   - odznaczamy opcję Export Everything
   - odznaczamy wszystkie opcje oprócz Reports (Access Events nie da się odznaczyć)
   - klikamy przycisk Export
   - klikamy przycisk Continue Export w oknie z ostrzeżeniem
5. Zmieniamy nazwe pobranego pliku na reports.zip i kopiujemy do folderu /import (nadpisujemy wcześniejszy plik)
6. Commitujemy zmianę w repozytorium

### Aktualizacja datasource
Przy pierwszym uruchomieniu do JasperServer importowany jest datasource "CMADataSourceJDBC". 
Aktualizacje datasource przeprowadzamy tylko na serwerze Jaspera w panelu administratora.

### Domyslny użytkownik do integracji z cma
Przy pierwszym uruchomieniu w JasperServer tworzony jest użytkownik cma z hasłem cma. 
Aktualizacja hasła odbywa sie tylko przez panel administratora. 
Należy potem pamiętać o zaktualizowaniu parametru SEUP_CMA_JASPER_PASSWORD w kontenerze seup-ma.


### Dokumentacja JasperReports Server
Dokumentacja znajduje się pod adresem https://community.jaspersoft.com/documentation?version=59011


